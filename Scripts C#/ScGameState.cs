﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScGameState : MonoBehaviour
{
    public Font Fuente;
    public static bool graficosP = true;
    public static bool graficosA = true;
    public static int puntos = 0;
    public static int vida = 10;
    public static int timer = 201;
    public static int limiteVida = 15;
    public Transform vidaobj;
    public Transform monedaobj;
    public Transform mejoraobj;
    public Transform limiteobj;
    private int aleatorio = 0;
    private bool check = false;
    public Transform rellenoVidaobj;
    private int aleatorio2 = 0;
    private bool check2 = false;
    public Transform AddTimeobj;
    public static int nivelgeneral = 1;
    public static int niveldes = 1;
    ////////////////////
    public Text disparoText;
    public Text tiempoText;
    public Text nivelText;
    public Text vidasText;
    public Text puntosText;
    void Start()
    {
        ////////////////////////////////////////
        InvokeRepeating("tiempo", 1.0f, 1.0f);
        InvokeRepeating("vidasobj", 1.0f, 30.0f);
        if (nivelgeneral != 5 && nivelgeneral != 15)
        {
            print(nivelgeneral);
            InvokeRepeating("mejora", 1.0f, 60.0f);
            InvokeRepeating("AddTimeobj1", 1.0f, 80.0f);
        }
        InvokeRepeating("monedaobj1", 1.0f, 20.0f);
        aleatorio = Random.Range(125, 75);
        aleatorio2 = Random.Range(100, 50);
        check = false;
    }

    void Update()
    {
        //Menu Top
        string tiempo = string.Format(" {00}", timer);
        string puntuacion = string.Format(" {00}", puntos);
        string nivelacion = string.Format(" {00}", nivelgeneral);
        string salud = string.Format(" {00}", vida);
        string triple = string.Format(" {00}", Disparo.restante);
        tiempoText.text = tiempo;
        puntosText.text = puntuacion;
        nivelText.text = nivelacion;
        vidasText.text = salud + "/" + limiteVida.ToString();
        disparoText.text = triple;
        //extencion de vidas
        if (vida > limiteVida)
        {
            vida = limiteVida;
        }
        if (timer == aleatorio)
        {
            if (check == false)
            {
                Instantiate(limiteobj, new Vector3(Random.Range(40, 50), Random.Range(0.8f, 10), 0), transform.rotation);
                check = true;
            }
        }
        if (timer == aleatorio2)
        {
            if (check2 == false)
            {
                Instantiate(rellenoVidaobj, new Vector3(Random.Range(40, 50), Random.Range(0.8f, 10), 0), transform.rotation);
                check2 = true;
            }
        }
        //condicion para el gameover
        if (timer < 196)
        {
            if (vida <= 0)
            {
                ScPlayer.disparotouch = false;
                Application.LoadLevel("perdernivel");
            }
        }
        if (timer <= 0)
        {
            ScPlayer.disparotouch = false;
            if (nivelgeneral == 1 || nivelgeneral == 3 || nivelgeneral == 9)
            {
                Application.LoadLevel("Publicidad");
            }
            else
            {
                Application.LoadLevel("ganarnivel1");
            }
        }
    }
    //establecemos un tiempo de juego
    void tiempo()
    {
        timer -= 1;
        CuentaRegresiva.tiempo1 = timer;
        if (timer <= 0)
        {
            CancelInvoke();
        }
    }
    //puntos
    public void AddScore()
    {
        puntos += 5;
    }
    public void AddScore2()
    {
        puntos += 10;
    }
    //vidas
     public void restavidas1()
    {
        vida -= 1;
    }
    public void restavidas2()
    {
        vida -= 2;
    }
    public void restavidas3()
    {
        vida -= 3;
    }
    public void restavidas4()
    {
        vida -= 4;
    }
    public void restavidas5()
    {
        vida -= 5;
    }
    public void sumarvidas1()
    {
        vida += 5;
    }
    public void sumarpuntos()
    {
        puntos += 50;
    }
    void vidasobj()
    {
        Instantiate(vidaobj, new Vector3(Random.Range(40, 50), Random.Range(0.8f, 10), 0), transform.rotation);
    }
    void monedaobj1()
    {
        Instantiate(monedaobj, new Vector3(Random.Range(40, 50), Random.Range(0.8f, 10), 0), transform.rotation);
    }
    void mejora()
    {
        Instantiate(mejoraobj, new Vector3(Random.Range(40, 50), Random.Range(0.8f, 10), 0), transform.rotation);
    }
    void AddTimeobj1()
    {
        Instantiate(AddTimeobj, new Vector3(Random.Range(40, 50), Random.Range(0.8f, 10), 0), transform.rotation);
    }

}