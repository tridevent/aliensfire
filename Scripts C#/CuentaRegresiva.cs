﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CuentaRegresiva : MonoBehaviour
{
    public static int tiempo1 = 200;
    public Sprite text1;
    public Sprite text2;
    public Sprite text3;
    public Sprite text4;
    void Start()
    {
        GetComponent<Image>().enabled = true;
    }
    void Update()
    {
        if (tiempo1 == 200)
        {
            GetComponent<Image>().sprite = text1;
        }
        if (tiempo1 == 199)
        {
            GetComponent<Image>().sprite = text2;
        }
        if (tiempo1 == 198)
        {
            GetComponent<Image>().sprite = text3;
        }
        if (tiempo1 == 197)
        {
            GetComponent<Image>().sprite = text4;
        }
        if (tiempo1 == 196)
        {
            GetComponent<Image>().enabled = false;
        }
    }
}
