﻿using UnityEngine;
using System.Collections;

public class EnemigoPelea : MonoBehaviour
{
    public bool TipoNave = false;//false 1 true 2
    public int vel = 10;
    private int comienzo = 50;
    private int fin = 125;
    public static int vidapelea = 15;
    public GameObject game1;
    public Transform explosion2;
    public AudioClip sonidoDisparo;
    public Transform disparo;
    public Transform arma1;
    private int contador = 0;
    //////escudo/////////////////
    public static bool escudo;
    private int contEscudo = 0;
    public Texture sano;
    public Texture medio;
    public Texture roto;
    private int contAnimacion = 0;
    private int i = 0;
    private int altura = 0;
    private int altura2 = 0;
    private int direccion = 1;
    private int contPosicion = 0;
    private int j = 0;
    ////////Enemigo 2 ////////////
    public Transform arma2;
    public Transform arma3;
    private int valorSano = 0;//9:20
    private int valorMedio = 0;//10:15
    private int valorRoto = 0;//5:10
    private bool comprobacion = false;
    //----------------------------//
    public GameObject escudoObj;
    public static bool check = false;

    void Start()
    {
        direccion = 1;
        escudo = true;
        InvokeRepeating("Escudo", 1, 1);
        InvokeRepeating("mover", 1, 0.01f);
        if (TipoNave == false)
        {
            InvokeRepeating("disparoEnemigo", 1, 0.01f);
            vidapelea = 15;
        }
        else
        {
            InvokeRepeating("disparoEnemigo", 1, 0.02f);
            vidapelea = 20;
        }
        check = false;
    }
    void Update()
    {
        if (comprobacion == false)
        {
            if (TipoNave == false)
            {
                valorSano = 9;
                valorMedio = 5;
                valorRoto = 3;
            }
            if (TipoNave == true)
            {
                valorSano = 20;
                valorMedio = 15;
                valorRoto = 5;
            }
            comprobacion = true;
        }
        if (vidapelea >= valorSano)
        {
            EscudoEnemigo.vidaEscudo = 3;
            GetComponent<Renderer>().material.mainTexture = sano;
        }
        if (vidapelea <= valorMedio && vidapelea > valorRoto)
        {
            print("medio");
            EscudoEnemigo.vidaEscudo = 2;
            GetComponent<Renderer>().material.mainTexture = medio;
        }
        if (vidapelea <= valorRoto)
        {
            EscudoEnemigo.vidaEscudo = 1;
            GetComponent<Renderer>().material.mainTexture = roto;
        }
        if (vidapelea == 0)
        {
            //***//
            check = true;
            vidapelea = 15;

            if (ScGameState.nivelgeneral == 15)
            {
                Application.LoadLevel("creditosfinal");
            }
            else
            {
                Application.LoadLevel("ganarnivel1");
            }
        }
        ////////////////////////////////////////////////////
        if (transform.position.y >= ScRoca.inicio)
        {
            transform.position = new Vector3 (transform.position.x, ScRoca.inicio, transform.position.z);
        }
        if (transform.position.y <= ScRoca.fin2)
        {
            transform.position =  new Vector3 (transform.position.x, ScRoca.fin2 , transform.position.z);
        }
        /////////////////////////////////////////////////////
    }
    void OnTriggerEnter(Collider other)
    {
        if ((other.gameObject.tag == "disparo" && escudo == false) || other.gameObject.tag == "escudo" || other.gameObject.tag == "misilobj")
        {
            ///////////////////////////////////////////////////////////////////////////////
            if (CuentaRegresiva.tiempo1 <= 196)
            {
                game1.transform.GetComponent<ScGameState>().AddScore2();
            }
            ////////////////////////////////////////////////////////////////////////////////
            if (ScGameState.graficosP == true)
            {
                Instantiate(explosion2, transform.position, transform.rotation);
            }
            vidapelea--;
            AudioSource.PlayClipAtPoint(sonidoDisparo, transform.position);
        }
        if (other.gameObject.tag == "jugador")
        {
            ///////////////////////////////////////////////////////////////////////////////
            Application.LoadLevel("perdernivel");
            ////////////////////////////////////////////////////////////////////////////////
        }
    }
    void disparoEnemigo()
    {
        if (CuentaRegresiva.tiempo1 <= 196)
        {
            contador++;
            if (contador == 100)
            {
                if (TipoNave == false)
                {
                    Instantiate(disparo, arma1.position, disparo.rotation);
                }
                else
                {
                    Instantiate(disparo, arma1.position, arma1.rotation);
                    Instantiate(disparo, arma2.position, arma2.rotation);
                    Instantiate(disparo, arma3.position, arma3.rotation);
                }
                contador = 0;
            }
        }
    }
    void Escudo()
    {
        contEscudo++;
        if (contEscudo >= 10)
        {
            escudo = !escudo;
            contEscudo = 0;
        }
        if (escudo == true)
        {
            escudoObj.SetActive(true);
        }
        if (escudo == false)
        {
            escudoObj.SetActive(false);
        }
    }
    void mover()
    {
        altura = Random.Range(9, 15);
        altura2 = Random.Range(3, -5);
        contPosicion++;
        if (contPosicion > 200)
        {
            transform.Translate(new Vector3(0, 0, direccion) * vel * Time.deltaTime);
            if (transform.position.y >= altura)
            {
                direccion = 1;
                j++;
            }
            if (transform.position.y <= altura2)
            {
                direccion = -1;
                j++;
            }
        }
        if (j == 5)
        {
            j = 0;
            contPosicion = 0;
        }
    }
}
