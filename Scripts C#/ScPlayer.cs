﻿using UnityEngine;
using System.Collections;

public class ScPlayer : MonoBehaviour
{

    public GameObject nave;//Nave Enemigo
    public float speed = 10.0f;
    public static bool disparotouch = false;
    public static bool cerradura = true;
    public Transform arma1;
    public int contar = 0;
    public Vector3 posiciontomada;
    public GameObject escudo;
    public static bool entra = false;
    public bool reset = false;
    ////////////////////////
    public RaycastHit hit;
    public Vector3 pos;
    public Touch touch;
    public bool movact;
    ///////////////////////
    public Texture sano;
    public Texture roto;
    ///////////////////////
    public static float movX;
    public static float movY;

    void Start()
    {
        GetComponent<Renderer>().material.mainTexture = sano;
        disparotouch = false;
        cerradura = true;
        escudo.SetActive(false);
        posiciontomada = new Vector3(5, 5, 0);
        transform.position = posiciontomada;
        nave.SetActive(false);
    }

    void Update()
    {
        if (Input.GetButton("subir"))
        {
            transform.position= new Vector3 (transform.position.x,transform.position.y + 0.3f, transform.position.z);
        }
        //----------------------------------------------------------------	
        if (Input.GetButton("bajar"))
        {
            transform.position = new Vector3 (transform.position.x,transform.position.y - 0.3f, transform.position.z);
        }
        //----------------------------------------------------------------	
        if (Input.GetButton("derecha"))
        {
            bool check3 = false;
            transform.position = new Vector3(transform.position.x + 0.3f,transform.position.y , transform.position.z);
            if (check3 == false)
            {
                transform.rotation =Quaternion.Euler(0,0,0.1f);
                check3 = true;
            }
        }
        if (Input.GetButtonUp("derecha"))
        {
            bool check5 = false;
            if (check5 == false)
            {
                transform.rotation= Quaternion.Euler(0, 0, 0);
                check5 = true;
            }
        }
        //----------------------------------------------------------------	
        if (Input.GetButton("izquierda"))
        {
            bool check4 = false;
            transform.position=new Vector3(transform.position.x - 0.3f, transform.position.y,transform.position.z);
            if (check4 == false)
            {
                transform.rotation= Quaternion.Euler(0, 0, - 0.05f);
                check4 = true;
            }
        }
        if (Input.GetButtonUp("izquierda"))
        {
            bool check6 = false;
            if (check6 == false)
            {
                transform.rotation= Quaternion.Euler(0, 0, 0);
                check6 = true;
            }
        }
        //----------------------------------------------------------------	
        if (transform.position.y <= 1)
        {
            transform.position =new Vector3(transform.position.x,1, transform.position.z) ;
        }
        if (transform.position.y >= 12)
        {
            transform.position= new Vector3(transform.position.x, 12, transform.position.z);
        }
        if (transform.position.x <= 5)
        {
            transform.position = new Vector3(5,transform.position.y,transform.position.z);
        }
        if (transform.position.x >= 25)
        {
            transform.position = new Vector3(25, transform.position.y, transform.position.z); ;
        }
        /////////////////////////////
        if (ScGameState.vida > 5)
        {
            GetComponent<Renderer>().material.mainTexture = sano;
        }
        if (ScGameState.vida < 5)
        {
            GetComponent<Renderer>().material.mainTexture = roto;
        }
        /////////////////////////////realizamos la vibracion para evitar el problema de colision
        contar += 1;
        if (contar > 2)
        {
            contar = 0;
        }
        if (contar == 1)
        {
            float negativo=0;
            negativo -= 0.01f;
            transform.Translate(0, negativo, 0);
        }
        if (contar == 2)
        {
            float transV = 0.01f;
            transform.Translate(0, transV, 0);
        }
        /////////////////////////////////////////////////////////////////////////////////////
        if (NivelGanar.nivel == true && (ScGameState.nivelgeneral < 4 || ScGameState.nivelgeneral == 5))
        {
            if (ScGameState.timer == 30 || ScGameState.timer == 60 || ScGameState.timer == 90 || ScGameState.timer == 120 || ScGameState.timer == 150 || ScGameState.timer == 180)
            {
                nave.SetActive(true);
            }
        }
        //////////////////////////////////////////////////////////
        if (entra == false)
        {
            escudo.SetActive(false);
        }
        //////////////////////////////////////////////////////////////////////////////////////////
        if (Input.GetButtonDown("Jump"))
        {
            cerradura = false;
        }
        if (Input.touchCount > 0)
        {
            if (Input.touchCount > 1 && disparotouch == false)
            {
                cerradura = false;
            }
            touch = Input.touches[0];
            Ray ray = Camera.main.ScreenPointToRay(Input.touches[0].position);
            if (touch.phase == TouchPhase.Began)
            {
                if (Input.touchCount > 1 && disparotouch == false)
                {
                    cerradura = false;
                }
                if (hit.collider.name != "Single Joystick" && hit.collider.name != "ToqueControl" && disparotouch == false)
                {
                    cerradura = false;
                }
                if (Input.touchCount > 1 && disparotouch == false)
                {
                    cerradura = false;
                }
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
    }


    void subir()
    {
        transform.position += new Vector3 (0, movY , 0);
    }
    //----------------------------------------------------------------	
    void bajar()
    {
        transform.position += new Vector3(0,movY,0);
    }
    //----------------------------------------------------------------	
    void derecha()
    {
        transform.position +=new Vector3 (movX,0,0);
    }
    //----------------------------------------------------------------	
    void izquierda()
    {
        transform.position += new Vector3 (movX,0,0);
    }
    //----------------------------------------------------------------	
}





















