﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CongelamientoGUI : MonoBehaviour
{
    public static bool congelamientoAct;
    public Sprite activo;
    public Sprite inactivo;
    public bool marcador = false;
    public  Image Imagen;

    void Start()
    {
        congelamientoAct = false;
    }

    void Update()
    {

    }
    public void BotonPulsado()
    {
        if (PausaGUI.cerradura == false && CuentaRegresiva.tiempo1 <= 196 && DobleTouch.temporizador > 0)
        {
            if (Tienda.congelamiento == true)
            {
                congelamientoAct = true;
                marcador = true;
                Tienda.congelamiento = false;
            }
        }
        if (marcador == false && Tienda.congelamiento == true)
        {
            GetComponent<Image>().sprite = activo;
        }
        if (marcador == true || Tienda.congelamiento == false)
        {
            GetComponent<Image>().sprite = inactivo;
        }
    }
}
