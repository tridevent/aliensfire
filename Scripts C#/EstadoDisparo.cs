﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EstadoDisparo : MonoBehaviour {
    public Sprite solido;
    public Sprite transparente;

	void Start () {
	
	}
	
	void Update () {
        if (ScPlayer.disparotouch == false) 
        {
            GetComponent<Image>().sprite = solido;
        }
        if (ScPlayer.disparotouch == true)
        {
            GetComponent<Image>().sprite = transparente;
        }
    }
}
