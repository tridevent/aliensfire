﻿using UnityEngine;
using System.Collections;

public class ScRoca : MonoBehaviour
{
    public static int comienzo = 35;
    public static int fin = 100;
    public static float inicio = 14;
    public static float fin2 = -4;
    public static int rocvel = 3;
    public static int rocvel1 = 15;
    public static int rocvel2 = 10;
    public static int rocvel3 = 5;
    public static int rocvel4 = 3;
    public int RocaTipo = 0;
    public Transform explosion2;
    public GameObject prefab;
    public GameObject game1;
    public AudioClip sonidoDisparo;
    public Vector3 reset1;
    public int copia;
    public bool check = false;

    void Start()
    {
        rocvel = 10;
        rocvel1 = 20;
        rocvel2 = 15;
        rocvel4 = 5;
        comienzo = 35;
        fin = 100;

    }


    void Update()
    {
        if (CongelamientoGUI.congelamientoAct == true)
        {
            if (check == false)
            {
                copia = ScGameState.timer;
                check = true;
            }
            if ((copia - ScGameState.timer) == 30)
            {
                CongelamientoGUI.congelamientoAct = false;
            }
        }
        if (CongelamientoGUI.congelamientoAct == true)
        {
            rocvel = rocvel - 5;
            CongelamientoGUI.congelamientoAct = false;
        }
        if (CuentaRegresiva.tiempo1 <= 196)
        {
            if (transform.position.y >= inicio)
            {
                transform.position = new Vector3 (transform.position.x,inicio, transform.position.z);
            }
            if (transform.position.y <= fin2)
            {
                transform.position= new Vector3(transform.position.x, fin2, transform.position.z);
            }
            if (ScGameState.timer >= 0 && ScGameState.timer <= 80)
            {
                if (CongelamientoGUI.congelamientoAct == true)
                {
                    transform.Translate(new Vector3(-1, 0, 0) * rocvel2 * Time.deltaTime);
                }
                if (CongelamientoGUI.congelamientoAct == false)
                {
                    transform.Translate(new Vector3(-1, 0, 0) * rocvel1 * Time.deltaTime);
                }
            }
            if (ScGameState.timer > 80 && ScGameState.timer <= 140)
            {
                if (CongelamientoGUI.congelamientoAct == true)
                {
                    transform.Translate(new Vector3(-1, 0, 0) * rocvel3 * Time.deltaTime);
                }
                if (CongelamientoGUI.congelamientoAct == false)
                {
                    transform.Translate(new Vector3(-1, 0, 0) * rocvel2 * Time.deltaTime);
                }
            }
            if (ScGameState.timer > 140 && ScGameState.timer <= 200)
            {
                if (CongelamientoGUI.congelamientoAct == true)
                {
                    transform.Translate(new Vector3(-1, 0, 0) * rocvel4 * Time.deltaTime);
                }
                if (CongelamientoGUI.congelamientoAct == false)
                {
                    transform.Translate(new Vector3(-1, 0, 0) * rocvel3 * Time.deltaTime);
                }
            }
            //resetamos la posicion de las rocas
            if (transform.position.x <= -5)
            {
                //creamos una roca aletoriamente
                reset1 = new Vector3(Random.Range(comienzo, fin), Random.Range(inicio, fin2), 0);
                transform.position = reset1;
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.tag == "jugador")
        {
            ///////////////////////////////////////////////////////////////////////////////
            if (CuentaRegresiva.tiempo1 <= 196)
            {
                if (RocaTipo == 1)
                {
                    game1.transform.GetComponent<ScGameState>().restavidas1();
                }
                if (RocaTipo == 2)
                {
                    game1.transform.GetComponent<ScGameState>().restavidas2();
                }
                if (RocaTipo == 3)
                {
                    game1.transform.GetComponent<ScGameState>().restavidas3();
                }
                if (RocaTipo == 4)
                {
                    game1.transform.GetComponent<ScGameState>().restavidas4();
                }
                if (RocaTipo == 5)
                {
                    game1.transform.GetComponent<ScGameState>().restavidas5();
                }
            }
            ////////////////////////////////////////////////////////////////////////////////
            if (ScGameState.graficosP == true)
            {
                Instantiate(explosion2, transform.position, transform.rotation);
            }
            reset1 = new Vector3(Random.Range(comienzo, fin), Random.Range(inicio, fin2), 0);
            transform.position = reset1;
            AudioSource.PlayClipAtPoint(sonidoDisparo, transform.position);

        }


    }

    //////////---********************************************************************************---//////////////

}
