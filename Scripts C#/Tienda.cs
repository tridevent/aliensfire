﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Tienda : MonoBehaviour
{
    public static bool misil;
    public static bool escudo;
    public static bool congelamiento;
    ////////////////////////////////////
    public Transform puntosobj;
    public static int misilprecio;
    public static int escudoprecio;
    public static int congelamientoprecio;
    ////////////////////////////////////
    public GameObject congelamientoobj;
    public GameObject misilobj;
    public GameObject escudoobj;
    public static bool contador1;
    public Sprite activo;
    public Sprite inactivo;
    public Text puntosText;

    void Start()
    {
        misilprecio = 3000;
        congelamientoprecio = 1000;
        escudoprecio = 5000;
    }

    void Update()
    {

        if (congelamiento == true)
        {
            congelamientoobj.GetComponent<Image>().sprite= activo;
        }
        if (congelamiento == false)
        {
            congelamientoobj.GetComponent<Image>().sprite= inactivo;
        }
        if (misil == true)
        {
            misilobj.GetComponent<Image>().sprite = activo;
        }
        if (misil == false)
        {
            misilobj.GetComponent<Image>().sprite = inactivo;
        }
        if (escudo == true)
        {
            escudoobj.GetComponent<Image>().sprite = activo;
        }
        if (escudo == false)
        {
            escudoobj.GetComponent<Image>().sprite = inactivo;
        }
        //definimos el estado de los poderes////////////////////////////////////////////

        //definimos cuando los puntos no son suficientes para la adquisicion del poder
        if (congelamientoprecio > ScGameState.puntos)
        {
            Global.dineroins1 = true;
        }
        if (misilprecio > ScGameState.puntos)
        {
            Global.dineroins2 = true;
        }
        if (escudoprecio > ScGameState.puntos)
        {
            Global.dineroins3 = true;
        }

        //definimos cuando los puntos son suficientes para adquirir el poder
        if (congelamientoprecio <= ScGameState.puntos)
        {
            Global.dineroins1 = false;
        }
        if (misilprecio <= ScGameState.puntos)
        {
            Global.dineroins2 = false;
        }
        if (escudoprecio <= ScGameState.puntos)
        {
            Global.dineroins3 = false;
        }
        //renderizado de GUI
        string tiempo = string.Format("{00}", ScGameState.puntos);
        puntosText.text = ScGameState.puntos.ToString();
    }
}
