﻿using UnityEngine;
using System.Collections;

public class PausaGUI : MonoBehaviour
{
    public static bool cerradura = false;
    public static bool bandera;
    public Touch toque;
    public GameObject reanudar;
    public GameObject regresar;
    public GameObject personaje;
    public GUITexture Imagen;
    //private bool  BotonPulsado = false;
    void Start()
    {
        cerradura = false;
        bandera = true;
        reanudar.SetActive(false);
        regresar.SetActive(false);
        personaje.SetActive(true);
    }

    void Update()
    {
        if (cerradura == false)
        {
            reanudar.SetActive(false);
            regresar.SetActive(false);
            personaje.SetActive(true);
            Time.timeScale = 1.0f;
            Sonido.sonido = true;
            //cerradura=true;
            bandera = true;
        }
        if (cerradura == true)
        {
            ScPlayer.disparotouch = false;
            Time.timeScale = 0.0f;
            personaje.SetActive(false);
            reanudar.SetActive(true);
            regresar.SetActive(true);
            Sonido.sonido = false;
            bandera = false;
        }
        //----------------------------------------------------------			
    }

    public void BotonPresionado()
    {
        cerradura = true;
    }

}
