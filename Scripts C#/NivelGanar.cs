﻿using UnityEngine;
using System.Collections;

public class NivelGanar : MonoBehaviour
{
    public static bool nivel = false;
    public int numeroNivel = 0;

    void Awake()
    {
        ScGameState.nivelgeneral = numeroNivel;
        print(ScGameState.nivelgeneral);
    }
    void Start()
    {
        nivel = true;
    }

    void Update()
    {
        if ((ScGameState.timer == 0 || EnemigoPelea.check == true) && nivel == true)
        {
            ScGameState.nivelgeneral = ScGameState.nivelgeneral + 1;
            //----------------------------//
            ScGameState.timer = 201;
            EnemigoPelea.check = false;
            nivel = false;
            //-----------------------------//
            if (ScGameState.nivelgeneral > ScGameState.niveldes)
            {
                ScGameState.niveldes = ScGameState.nivelgeneral;
            }
        }
    }

}