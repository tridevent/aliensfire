﻿using UnityEngine;
using System.Collections;

public class EscudoEnemigo : MonoBehaviour
{
    public static int vidaEscudo = 3;
    public Color sano;
    public Color medio;
    public Color roto;
    void Start()
    {
        vidaEscudo = 3;
    }

    void Update()
    {
        if (vidaEscudo == 3)
        {
            GetComponent<Renderer>().material.color = sano;
        }
        if (vidaEscudo == 2)
        {
            GetComponent<Renderer>().material.color = medio;
        }
        if (vidaEscudo == 1)
        {
            GetComponent<Renderer>().material.color = roto;
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "jugador")
        {
            Application.LoadLevel("perdernivel");
        }
    }
}