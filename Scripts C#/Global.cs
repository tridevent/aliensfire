﻿using UnityEngine;
using System.Collections;

public class Global : MonoBehaviour
{
    private int entrada = 0;
    
    ///////////////////////////////////////////////////////
    public static float x = Screen.width;
    public static float y = Screen.height;
    //----------------------------------------------------//
    public static bool cambio = false;//para verificar si entro en la escena ganar
    public static bool cambioperder = false;//para verificar si entro en la escena perder
    //---------------------------------------------------//
    public static bool dineroins1;
    public static bool dineroins2;
    public static bool dineroins3;
    public static bool dinerosuf;

    void Start()
    {
        //TODO: hacer que este codigo funcione, ya que no deja ponerlo antes del start.
        entrada = PlayerPrefs.GetInt("Entrada");
        ScGameState.puntos = PlayerPrefs.GetInt("Puntos");
        //ScGameState1.puntos=15000;
        ScGameState.vida = PlayerPrefs.GetInt("Vida");
        ScGameState.limiteVida = PlayerPrefs.GetInt("LimiteVida");
        if (entrada == 0)
        {
            PlayerPrefs.DeleteAll();
            ScGameState.vida += 10;
            ScGameState.limiteVida += 15;
            PlayerPrefs.SetInt("Vida", ScGameState.vida);
            PlayerPrefs.SetInt("LimiteVida", ScGameState.limiteVida);
            entrada = 1;
        }
        PlayerPrefs.SetInt("Entrada", entrada);
        //ScGameState1.puntos = 15000;
        ScGameState.niveldes = PlayerPrefs.GetInt("Nivel");
        //**********************************************************************************
        x = Screen.width;
        y = Screen.height;
        cambio = false;
        cambioperder = false;
    }

}
